function createCard(name, description, pictureUrl, startDateElement, endDateElement) {
    return `
    <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
            <small class="card-text">${startDateElement} - ${endDateElement}</small>
    </div>
    `;
}



window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
    const response = await fetch(url);
    if (!response.ok) {
        console.log("Something went wrong...")// Figure out what to do when the response is bad
    } else {
        const data = await response.json();

        const columns = document.querySelectorAll(".col");

        for (let conference of data.conferences){
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const starts = new Date(details.conference.starts);
                const ends = new Date(details.conference.ends);
                const startDateElement = starts.toLocaleDateString("en-US", { month: 'numeric', day: 'numeric', year: 'numeric' });
                const endDateElement = ends.toLocaleDateString("en-US", { month: 'numeric', day: 'numeric', year: 'numeric' })
                const html = createCard(name, description, pictureUrl, startDateElement, endDateElement);
                columns.forEach(column => {
                    column.innerHTML += html;
                });
            }

        }
    }
}   catch(error){
    console.error("An error occured: ", error);
    // Figure out what to do if an error is raised
 }
});
